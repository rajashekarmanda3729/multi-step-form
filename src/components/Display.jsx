import logo from '../assets/24609.jpg'
import Navbar from './Navbar'
import SignUp from './SignUp'
import Message from './Message'
import Checkbox from './Checkbox'
import { useSelector } from 'react-redux'

const Display = () => {

    const { signUp, message, checkBox } = useSelector(store => store.user)

    return (
        <section className='d-flex flex-row justify-content-center p-5 vh-100'>
            <section className='d-flex flex-row shadow-lg rounded-5 w-100'>
                <img src={logo} alt="login" className="w-25 rounded-5 h-100" />
                <section className='d-flex flex-column w-75 p-5'>
                    {!checkBox && <Navbar />}
                    {!signUp && <SignUp />}
                    {signUp && !message && <Message />}
                    {message && !checkBox && <Checkbox />}
                    {checkBox && <h1 className='text-primary text-center mt-5'>Submitted Successfully</h1>}
                    {/* {checkBox && alert('Submitted Successfully')} */}
                </section>
            </section>
        </section>
    )
}

export default Display