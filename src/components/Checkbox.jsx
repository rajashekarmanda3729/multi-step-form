import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { backToMessage, validateCheckbox, onChangeCheckbox, onCheckboxOptions } from '../redux/reducer/userSlice'

const Checkbox = () => {

    const dispatch = useDispatch()

    const { checkboxErrors, checkboxDetails } = useSelector(store => store.user)

    const handleCheckBox = (event) => {
        const name = event.target.name
        dispatch(onChangeCheckbox({ name: name }))
    }

    const handleOptions = (event) => {
        const data = event.target.value
        // console.log(data)
        dispatch(onCheckboxOptions({ name: data }))
    }

    return (
        <section className='p-4 d-flex flex-column justify-content-center align-items-center'>
            <div className="d-flex flex-column justify-content-start align-items-start w-100 mb-2">
                <p>Step 3/3</p>
                <h2>Checkbox</h2>
            </div>
            <section className='d-flex flex-column w-100 checkbox-field'>
                <section className='d-flex justify-content-start'>
                    <button className={`me-5 btn ${checkboxDetails.gender == 'male' && 'active border border-primary'}`} onClick={handleCheckBox} >
                        <img src="https://cdn.discordapp.com/attachments/1093516647110824027/1099311199218307135/Screenshot_from_2023-04-22_17-55-27.png"
                            className='image' alt="first" name='male' />
                    </button>
                    <button className={`btn ${checkboxDetails.gender == 'female' && 'active border border-primary'}`} onClick={handleCheckBox} >
                        <img src="https://cdn.discordapp.com/attachments/1093516647110824027/1099311199457390642/Screenshot_from_2023-04-22_17-55-39.png"
                            className='image' alt="first" name='female' />
                    </button>
                </section>
                <p className="text-danger ms-1" >{checkboxErrors.gender}</p>
            </section>
            <section className='options-check  d-flex flex-column justify-content-start align-items-start w-100'>
                <div className="d-flex mb-3">
                    <input type="radio" id='one' className=' border-0 rounded-circle' value='checkBoxFirstChoice' checked={checkboxDetails.choice == 'checkBoxFirstChoice'}
                        name='choice' onChange={handleOptions} />
                    <label htmlFor="one" className='ms-2 fs-5'>I want to add this option.</label>
                </div>
                <div className="d-flex">
                    <input type="radio" id='two' className='border-0 rounded-circle' value='checkBoxSecondChoice' checked={checkboxDetails.choice == 'checkBoxSecondChoice'}
                        name='choice' onChange={handleOptions} />
                    <label htmlFor="two" className='ms-2 fs-5'>Let me click on this checkbox and choose some cool stuff</label>
                </div>
            <p className="text-danger">{checkboxErrors.checkboxError}</p>
            </section>
            <hr className='w-100 border border-dark mb-5' />
            <div className='d-flex flex-row justify-content-end align-items-end w-100'>
                <button className="btn btn-light mb-5 btn-lg" onClick={() => dispatch(backToMessage())}>Back</button>
                <button className="btn btn-primary ms-5 mb-5 btn-lg" onClick={() => dispatch(validateCheckbox())}>Submit</button>
            </div>
        </section >
    )
}

export default Checkbox