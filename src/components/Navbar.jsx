import React from 'react'
import { useSelector } from 'react-redux'
import { ImCheckmark } from 'react-icons/im'

const Navbar = () => {

    const { signUp, message, checkBox, activeTab } = useSelector(store => store.user)

    return (
        <section className='d-flex flex-column w-100'>
            <section className='d-flex flex-row justify-content-evenly mt-2 p-2'>
                <div className=' d-flex flex-row justify-content-center align-items-center'>
                    <button className={`btn btn-${activeTab !== 'SignUp' ? 'light' : 'primary'}`}>{signUp ? <ImCheckmark color='blue' /> : 1}</button>
                    <h4 className='ms-3'>Sign Up</h4>
                </div>
                <div className='d-flex flex-row justify-content-center align-items-center'>
                    <button className={`btn btn-${activeTab !== 'Message' ? 'light' : 'primary'}`}>{message ? <ImCheckmark color='blue' /> : 2}</button>
                    <h4 className='ms-3'>Message</h4>
                </div>
                <div className='d-flex flex-row justify-content-center align-items-center'>
                    <button className={`btn btn-${activeTab !== 'Checkbox' ? 'light' : 'primary'}`}>{checkBox ? <ImCheckmark color='blue' /> : 3}</button>
                    <h4 className='ms-3'>Checkbox</h4>
                </div>
            </section>
            <hr className='border border-dark w-100' />
        </section>
    )
}

export default Navbar