import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { backToSignUp, onChangeMessage, onValidateMessage } from '../redux/reducer/userSlice'

const Message = () => {

    const { signUp, messageErrors, messageDetails } = useSelector(store => store.user)
    const dispatch = useDispatch()

    const handleMessage = (event) => {
        const data = event.target.value
        // console.log(data)
        dispatch(onChangeMessage({ name: event.target.name, value: data }))
    }

    return (
        <section className='p-4 d-flex flex-column justify-content-center align-items-center'>
            <div className="d-flex flex-column justify-content-start align-items-start w-100 mb-2">
                <p>Step 2/3</p>
                <h2>Message</h2>
            </div>
            <section className='d-flex flex-column fs-4 w-100 message-field'>
                <label htmlFor={`address ${messageErrors.address && 'text-danger'}`} >Address</label>
                <textarea type="text" placeholder='address' name='address' value={messageDetails.address}
                    onChange={handleMessage} rows={6} className='p-2' />
                <p className="text-danger fs-6">{messageErrors.address}</p>
            </section>
            <section className='d-flex flex-column w-100 message-option'>
                <div className='d-flex justify-content-evenly w-100'>
                    <div className="d-flex">
                        <input type="radio" name='choice' value='firstChoice' id='one' onChange={handleMessage}
                            checked={messageDetails.choice == 'firstChoice'}
                            className='border-0 rounded-circle' />
                        <label htmlFor="one" className='ms-2 fs-5'>The number one choice</label>
                    </div>
                    <div className="d-flex">
                        <input type="radio" name='choice' value='secondChoice' id='two' onChange={handleMessage}
                            checked={messageDetails.choice == 'secondChoice'}
                            className='border-0 rounded-circle' />
                        <label htmlFor="two" className='ms-2 fs-5'>The number two choice</label>
                    </div>
                </div>
                <p className="text-danger text-center fs-6">{messageErrors.choiceError}</p>
            </section>
            <hr className='w-100 border border-dark' />
            <div className='d-flex flex-row justify-content-end align-items-end w-100'>
                <button className="btn btn-light mb-5 btn-lg" onClick={() => dispatch(backToSignUp())}>Back</button>
                <button className="btn btn-primary ms-5 mb-5 btn-lg" onClick={() => dispatch(onValidateMessage())}>Next Step</button>
            </div>
        </section>
    )
}

export default Message