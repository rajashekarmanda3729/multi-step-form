import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { onStoreData, validateSignUp } from '../redux/reducer/userSlice'
import '../App.css'
import { VscInfo } from 'react-icons/vsc'

const SignUp = () => {

    const dispatch = useDispatch()

    const { userDetails, userErrors, signUp } = useSelector(store => store.user)
    const { firstName, lastName, email, dateOfBirth, address } = userDetails

    const handleChange = (event) => {
        const inputData = event.target.value
        dispatch(onStoreData({ name: event.target.name, value: inputData }))
    }

    // console.log(signUp)

    return (
        <section className='p-4 d-flex flex-column justify-content-center align-items-center h-100'>
            <div className="d-flex flex-column justify-content-start align-items-start w-100">
                <p>Step 1/3</p>
                <h2>Sign UP</h2>
            </div>
            <form className='w-100 h-75 d-flex flex-column justify-content-between align-items-center'>
                <section className='d-flex w-100'>
                    <field className='d-flex w-50 flex-column me-5 field'>
                        <label htmlFor="username" className={`fs-4 ${userErrors.firstName && 'text-danger'}`}>FirstName</label>
                        <input type="text" onChange={handleChange} className={`fs-4 p-2 text-dark ${userErrors.firstName && 'bg-danger bg-opacity-10 border-danger'}`}
                            id='username' placeholder='username' name='firstName' value={firstName} />
                        <p className="text-danger text-sm">{userErrors.firstName ? userErrors.firstName : ''}</p>
                    </field>
                    <field className='d-flex w-50 flex-column field'>
                        <label htmlFor="username" className={`fs-4 ${userErrors.firstName && 'text-danger'}`}>LastName</label>
                        <input type="text" id='username' onChange={handleChange} className={`fs-4 p-2 text-dark ${userErrors.firstName && 'bg-danger bg-opacity-10 border-danger'}`}
                            placeholder='username' name='lastName' value={lastName} />
                        <p className="text-danger">{userErrors.lastName}</p>
                    </field>
                </section>
                <section className='d-flex w-100'>
                    <field className='d-flex w-50 flex-column me-5 field'>
                        <label htmlFor="username" className={`fs-4 ${userErrors.firstName && 'text-danger'}`}>DateOfBirth</label>
                        <input type="date" id='username' onChange={handleChange} className={`fs-4 p-2 text-dark ${userErrors.firstName && 'bg-danger bg-opacity-10 border-danger'}`}
                            placeholder='username' name='dateOfBirth' value={dateOfBirth} />
                        <p className="text-danger">{userErrors.dateOfBirth}</p>
                    </field>
                    <field className='d-flex w-50 flex-column field'>
                        <label htmlFor="username" className={`fs-4 ${userErrors.firstName && 'text-danger'}`}>Email Address</label>
                        <input type="text" id='username' onChange={handleChange} className={`fs-4 p-2 text-dark ${userErrors.firstName && 'bg-danger bg-opacity-10 border-danger'}`}
                            placeholder='username' name='email' value={email} />
                        <p className="text-danger">{userErrors.email}</p>
                    </field>
                </section>
                <section className='d-flex flex-column fs-4 w-100 field'>
                    <label htmlFor="address" className={`${userErrors.firstName && 'text-danger'}`}>Address</label>
                    <input name='address' type="text" placeholder='address' value={address} className={`fs-5 p-2 text-dark ${userErrors.firstName && 'bg-danger bg-opacity-10 border-danger'}`}
                        onChange={handleChange} />
                    <p className="text-danger fs-6">{userErrors.address ? userErrors.address : ''}</p>
                </section>
            </form>
            <hr className='w-100 border border-dark' />
            <div className='d-flex flex-row justify-content-end align-items-end w-100'>
                <button type='button' onClick={() => dispatch(validateSignUp())}
                    className="btn btn-primary btn-lg">Next Step</button>
            </div>
        </section>
    )
}

export default SignUp


