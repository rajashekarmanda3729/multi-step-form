import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    activeTab: 'SignUp',
    userDetails: {
        firstName: '',
        lastName: '',
        email: '',
        address: '',
        dateOfBirth: '',
    },
    userErrors: {
        firstName: '',
        lastName: '',
        email: '',
        address: '',
        dateOfBirth: '',
    },
    messageDetails: {
        address: '',
        choice: ''
    },
    messageErrors: {
        address: '',
        choiceError: ''
    },
    checkboxDetails: {
        gender: '',
        choice: ''
    },
    checkboxErrors: {
        gender: '',
        checkboxError: ''
    },
    signUp: false,
    message: false,
    checkBox: false,
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        onStoreData: (state, { payload }) => {
            state.userDetails[payload.name] = payload.value
        },
        validateSignUp: (state, { payload }) => {
            (!state.userDetails.email.endsWith('@gmail.com')) ? state.userErrors.email = '* Please enter valid email-id' : state.userErrors.email = ''
            Object.keys(state.userDetails).map(each => {
                if (state.userDetails[each] === '') {
                    state.userErrors[each] = `* ${each} is required`
                } else {
                    state.userErrors[each] = ''
                }
            })
            let status = 0
            Object.keys(state.userErrors).map(error => {
                if (state.userErrors[error] == '') status++
            })
            if (status == 5) {
                state.signUp = true
                state.activeTab = 'Message'
            }
        },
        backToSignUp: (state) => {
            state.signUp = false
            state.activeTab = 'SignUp'
        },
        onChangeMessage: (state, { payload }) => {
            state.messageDetails[payload.name] = payload.value
        },
        onValidateMessage: (state) => {
            if (state.messageDetails.address == '') state.messageErrors.address = '* Message field is required'
            if (state.messageDetails.choice == '') state.messageErrors.choiceError = '* Please select any one choice'
            if (state.messageDetails.address !== '') state.messageErrors.address = ''
            if (state.messageDetails.choice !== '') state.messageErrors.choiceError = ''
            if (state.messageDetails.address !== '' && state.messageDetails.choice !== '') {
                state.message = true
                state.activeTab = 'Checkbox'
            }
        },
        backToMessage: (state) => {
            state.message = false
            state.activeTab = 'Message'
        },
        validateCheckbox: (state) => {
            if (state.checkboxDetails.gender == '') {
                state.checkboxErrors.gender = '* Please select gender'
            } else {
                state.checkboxErrors.gender = ''
            }
            if (state.checkboxDetails.choice == '') {
                state.checkboxErrors.checkboxError = '* Please select any one option'
            } else {
                state.checkboxErrors.checkboxError = ''
            }
            if (state.checkboxErrors.gender == '' && state.checkboxErrors.checkboxError == '') {
                state.checkBox = true
                state.activeTab = ''
            }
        },
        onChangeCheckbox: (state, { payload }) => {
            state.checkboxDetails.gender = payload.name
        },
        onCheckboxOptions: (state, { payload }) => {
            state.checkboxDetails.choice = payload.name
        }
    }
})

export const { onStoreData, validateSignUp, backToSignUp, onChangeMessage,
    onValidateMessage, backToMessage, validateCheckbox, onChangeCheckbox, onCheckboxOptions } = userSlice.actions

export default userSlice.reducer