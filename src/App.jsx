import Display from "./components/Display"


function App() {

  return (
    <section className=''>
      <Display />
    </section>
  )
}

export default App
